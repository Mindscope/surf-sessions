import { Component, OnInit } from '@angular/core';
import { remote, ipcRenderer } from 'electron';
import * as _ from 'lodash';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  dropzoneActive = false;
  files: FileList;

  constructor() {}

  ngOnInit() {}

  isAdvancedUpload = function() {
    const div = document.createElement('div');
    return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
  };

  closeWindow() {
    remote.getCurrentWindow().close();
  }

  minimizeWindow() {
    remote.getCurrentWindow().minimize();
  }

  handleDrop(files: FileList) {
    this.files = files;
  }

  handleHover(isHovering: boolean) {
    this.dropzoneActive = isHovering;
  }
}
